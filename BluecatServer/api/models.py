#!/usr/bin/env python
# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

OSCHOICE = (
    (0,'linux'),
    (1,'windows'),
)

NETWORK = (
    (0,'success'),
    (1,'failed'),
)


class RoomLocationModel(models.Model):
    """机房"""
    name = models.CharField(max_length=128)
    chname = models.CharField(max_length=128)
class OsTypeModel(models.Model):
    """操作系统"""
    name = models.CharField(max_length=128)
    type = models.CharField(choices=OSCHOICE,max_length=128)
    version = models.CharField(max_length=128)
    distribution = models.TextField()

class IdracModel(models.Model):
    """Idrac 信息"""
    username = models.CharField(max_length=128)
    password = models.CharField(max_length=128)
    
class FactoryModel(models.Model):
    """服务器厂商"""
    name = models.CharField(max_length=128)
    model = models.CharField(max_length=128)

class ServerModel(models.Model):
    """机器""" 
    bip = models.GenericIPAddressField()
    mip = models.GenericIPAddressField()
    roomlocation = models.ForeignKey('RoomLocationModel',on_delete=models.CASCADE)
    partitions =  models.TextField()
    os = models.ForeignKey('OsTypeModel',on_delete=models.CASCADE)
    vlanid = models.IntegerField()
    percent = models.IntegerField()
    status = models.CharField(max_length=128)
    reboot = models.IntegerField()
    hostname = models.CharField(max_length=128)
    comment = models.TextField()
    raid = models.TextField()
    network = models.IntegerField(choices=NETWORK)
    factory = models.ForeignKey('FactoryModel',on_delete=models.CASCADE)
    create_time = models.DateTimeField(auto_now=True)
    update_time = models.DateTimeField(auto_now=True)

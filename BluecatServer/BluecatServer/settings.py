#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
env = os.getenv('BLUECATENV','dev')
if env == 'prod':
   from prod import *
else:
   from dev import *

